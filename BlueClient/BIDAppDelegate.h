//
//  BIDAppDelegate.h
//  BlueClient
//
//  Created by Li Xianyu on 13-11-6.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
