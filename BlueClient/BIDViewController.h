//
//  BIDViewController.h
//  BlueClient
//
//  Created by Li Xianyu on 13-11-6.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BIDViewController : UIViewController <CBCentralManagerDelegate,CBPeripheralDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) NSMutableData *data;

@property (strong, nonatomic) IBOutlet UIButton *scanPeripherals;
- (IBAction)scanNowButton:(id)sender;

+(CAKeyframeAnimation *)getKeyframeAni;
@end
