//
//  BIDViewController.m
//  BlueClient
//
//  Created by Li Xianyu on 13-11-6.
//  Copyright (c) 2013年 Li Xianyu. All rights reserved.
//

#import "BIDViewController.h"

@interface BIDViewController ()

@end

@implementation BIDViewController

- (IBAction)scanNowButton:(id)sender {
    NSLog(@"%s", __func__);
    NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
                          [CBUUID UUIDWithString:@"1803"], nil];
    NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
//    [_manager scanForPeripheralsWithServices:nil options:nil];
    [_manager scanForPeripheralsWithServices:nil options:optionsdict];
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
//    _manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionRestoreIdentifierKey : @"myCentralManagerIdentifier"}];
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn:
            NSLog(@"CBCentralManagerStatePoweredOn");
            break;
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

/*!
 *一旦找到一个Peripheral，此函数就会被调用
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s", __func__);
    NSLog(@"Did discover peripheral. peripheral: %@ rssi: %@, UUID: %@ advertisementData: %@ ", peripheral.identifier, RSSI, peripheral, advertisementData);
    NSLog(@"UUID = %@", peripheral.identifier.UUIDString);
    
//    [self.manager stopScan];
    if (_peripheral != peripheral) {
        NSLog(@"Connecting to peripheral %@", peripheral);
        _peripheral = peripheral;
//        [self.manager connectPeripheral:peripheral options:nil];
    } else {
//        [self.manager connectPeripheral:peripheral options:nil];
    }
     
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [_data setLength:0];
    
    [_peripheral setDelegate:self];
    
    [_peripheral discoverServices:nil];
    
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s", __func__);
    if (error) {
        NSLog(@"Error discovering service: %@", [error localizedDescription]);
//        [self cleanup];
        return;
    }
    for (CBService *service in peripheral.services) {
        NSLog(@"Service found with UUID: %@", service.UUID);
        if ([service.UUID isEqual:@""]) {
            NSLog(@"Equal");
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s", __func__);
    //[_manager retrievePeripheralsWithIdentifiers:<#(NSArray *)#>
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)state {
    NSLog(@"%s", __func__);
    NSArray *peripherals = state[CBCentralManagerRestoredStatePeripheralsKey];
}
@end
